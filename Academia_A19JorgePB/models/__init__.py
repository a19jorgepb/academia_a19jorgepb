from . import academia_persona
from . import academia_estudiante
from . import academia_curso
from . import academia_aula
from . import academia_profesor
from . import academia_horario
