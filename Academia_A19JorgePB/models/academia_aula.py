import logging
from odoo import models, fields, api


class AcademiaAula(models.Model):
    _name = 'academia.aula'
    _description = 'Aula da acacemia'


    name = fields.Char('Nome Aula',required=True)
    description=fields.Text()
    ubicacion=fields.Text()

    _sql_constraints = [('name_uniq', 'UNIQUE (name)', 'Xa existe o nome da aula introducido')]


