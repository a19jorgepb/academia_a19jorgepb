
# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api
from datetime import datetime,date,time,timedelta

class AcademiaEstudiante(models.Model):
    _inherit='academia.persona'
    _name = 'academia.estudiante'
    _description = 'Estudiantes da acacemia'

    fecha_nacemento = fields.Date('Fecha nacemento')
    edad=fields.Integer('Edad', readonly=True, compute='_fecha_nac')

    @api.depends('fecha_nacemento','edad')
    def _fecha_nac(self):
        res=0
        if self.fecha_nacemento:
            self.edad = (datetime.now().date()-self.fecha_nacemento).days / 365
            if self.edad < 0:
                self.edad = 0
            res = self.edad 
        return res 


