import logging


from odoo import models, fields, api
from datetime import datetime,date,time,timedelta

class AcademiaCurso(models.Model):
    _name = 'academia.curso'
    _description = 'Curso da acacemia'


    name = fields.Char('Nome Curso',required=True)
    description=fields.Text('Descripción')
    aforo=fields.Integer(string="Aforo")
    aforo_ocupado=fields.Float(string="Aforo ocupado",compute='_aforo_ocupado')
    aula_id=fields.Many2one('academia.aula',ondelete='set null',string="Aula",index=True)
    profesor_id=fields.Many2one('academia.profesor',ondelete='restrict',string="Profesor",index=True,required=True)
    alumno_ids=fields.Many2many('academia.estudiante',string="Estudantes")
    alumnos_count = fields.Integer(string="Numero de alumnos",compute='_get_alumnos_count')
    
    horario_id =  fields.Many2many('academia.horario', required=True,)
    fecha_comenzo = fields.Date('Fecha comezo', required =True,readonly="1",default=fields.Date.today())
    fecha_fin = fields.Date('Fecha fin', readonly="1")
    state=fields.Selection([
        ('En curso','En curso'),
        ('Curso terminado','Curso terminado')],
        'State',default='En curso')

    is_terminado=fields.Boolean('Terminado',default=False, readonly="1")

    _sql_constraints = [('name_uniq2', 'UNIQUE (name)', 'O nome do curso non se pode repetir')]
    
    def make_terminado(self):
        self.cambiar_estado('Curso terminado')

    @api.model
    def permitida_transicion(self,old_state,new_state):
        allowed=[('En curso','Curso terminado')]
        return (old_state,new_state) in allowed

    @api.multi
    def cambiar_estado(self,new_state):
        for curso in self:
            if curso.permitida_transicion(curso.state,new_state):
                curso.state=new_state
                curso.fecha_fin=fields.Date.today()
                curso.is_terminado=True
                #curso.horario_id=self.env['academia.horario']
    

    @api.constrains('horario_id')
    def _check_horario_id(self):
        for horas in self:
            for h in self.horario_id:
                
                domain=['&','|',('horario_id.id', '=',h.id),'&',('profesor_id.id','=',horas.profesor_id.id),'&',('horario_id.dia','=',h.dia),('horario_id.hora','=',h.hora),('is_terminado','=',False)]
                #domain=['&','&',('horario_id.id', '=', h.id),('horario_id.hora','=',h.hora),'|','&',('fecha_comenzo', '<=', horas.fecha_comenzo),('fecha_fin', '>=', horas.fecha_comenzo),'&',('fecha_comenzo', '<=', horas.fecha_fin),('fecha_fin', '>=', horas.fecha_fin)]
                
                #domain = ['&','|',('horario_id.id', '=',h.id),'&',('profesor_id.id','=',horas.profesor_id.id),'&',('horario_id.dia','=',h.dia),('horario_id.hora','=',h.hora),'|',('fecha_fin', '>=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)]
                ocupado = self.search(domain, count=True) >  1
                if ocupado:
                    raise models.ValidationError('Error, horario ou horarios ocupados!')


    @api.depends('aforo','alumno_ids')
    def _aforo_ocupado(self):
        for r in self:
            if not r.aforo:
                r.aforo_ocupado=0.0
            else:
                r.aforo_ocupado=100*len(r.alumno_ids) / r.aforo


    @api.constrains('aforo','alumno_ids')
    def _verificar_aforo_valido(self):
        if self.aforo<0:
            raise models.ValidationError('O aforo non pode ser negativo')
        if self.aforo<len(self.alumno_ids):
            raise models.ValidationError('Incrementa o aforo ou elimina estudiantes')
    

    #@api.constrains('horario_id','is_terminado')
    #def _check_horarid_terminado(self):
        #for r in self:
            #if  r.is_terminado==True:
                #raise models.ValidationError('Non podes engadir horarios a un curso terminado')


    #@api.constrains('horario_id')
    #def _check_horario_id(self):
     #   for horas in self:
      #      for h in self.horario_id:
                #domain = ['|',('fecha_fin', '>=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)]
                #domain = ['&','|',('horario_id.id', '=',h.id),('profesor_id.id','=',horas.profesor_id.id),'|',('fecha_fin', '>=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)]
                #domain = ['&','|',('horario_id.id', '=',h.id),'&',('profesor_id.id','=',horas.profesor_id.id),('horario_id.hora','=',h.hora),'|',('fecha_fin', '>=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)]
                #domain=['|',('fecha_fin', '>=', horas.fecha_fin),('fecha_fin', '>=', horas.fecha_comenzo)]
                #domain = ['&','|',('horario_id.id', '=',h.id),'&',('profesor_id.id','=',horas.profesor_id.id),('horario_id.hora','=',h.hora),'|',('fecha_fin', '<=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)
                
       #         domain=['&','&',('horario_id.id', '=', h.id),('horario_id.hora','=',h.hora),'|','&',('fecha_comenzo', '<=', horas.fecha_comenzo),('fecha_fin', '>=', horas.fecha_comenzo),'&',('fecha_comenzo', '<=', horas.fecha_fin),('fecha_fin', '>=', horas.fecha_fin)]
                
                #domain = ['&','|',('horario_id.id', '=',h.id),'&',('profesor_id.id','=',horas.profesor_id.id),'&',('horario_id.dia','=',h.dia),('horario_id.hora','=',h.hora),'|',('fecha_fin', '>=', horas.fecha_fin),('fecha_comenzo', '<=', horas.fecha_fin)]
                #domain = ['&',('horario_id.id', '=', h.id),('fecha_fin', '>=', horas.fecha_fin)]
                #domain = ['&',('horario_id.id', '=', horas.horario_id.id),'|',('fecha_fin', '>', horas.fecha_fin),('fecha_comenzo','<',horas.fecha_comenzo)]
                # Comprueba que hay más de 1 registro pues el record que se está creando también cuenta aunque no esté todavía en la BBDD
        #        ocupado = self.search(domain, count=True) >  1
         #       if ocupado:
          #          raise models.ValidationError('Error, horario ocupado!')
    
    #@api.constrains('fecha_fin', 'fecha_comenzo')
    #def _check_dates(self):
       
     #   for curso in self:
            #raise models.ValidationError(curso.fecha_fin)
            #domain1=['&',(self.fecha_comenzo<curso.fecha_fin),(self.fecha_comenzo>curso.fecha_comenzo)]
      #      if self.fecha_comenzo<curso.fecha_fin and self.fecha_comenzo>curso.fecha_comenzo:
       #         raise models.ValidationError(curso.fecha_fin)
        #        FC = self.fecha_comenzo
         #       FF = curso.fecha_fin
                #raise models.ValidationError(FC)
          #      if self.fecha_comenzo>curso.fecha_comenzo:
           #         FC = curso.fecha_comenzo
            #        FF = self.fecha_fin
             #   if (FF-FC).days>7:
              #      for horario in self.horario_id:
               #         domain=[('horario_id.id', '=', horario.id)]
                #        ocupado = self.search(domain, count=True) >  1
                 #       if ocupado:
                  #         raise models.ValidationError((FF-FC).days)
                #else:
                 #   for i in range((FF-FC).days):
                  #      FI =FC + timedelta(days=i)
                   #     dia_semana=datetime.weekday(FI)
                    #    tupla_dias=("Lunes","Martes","Miercoles","Jueves","Viernes","Sabado","Domingo")
                     #   for horario in self.horario_id:
                      #      if tupla_dias[dia_semana] == horario.dia:
                       #         hora=horario.hora
                        #        for horario_curso in curso.horario_id:
                         #           if tupla_dias[dia_semana]==horario_curso.dia and horario_curso.hora==hora:
                          #              raise models.ValidationError('Error, horario ocupado!asda')



