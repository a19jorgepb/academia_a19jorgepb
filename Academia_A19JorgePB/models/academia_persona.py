# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api


class AcademiaPersona(models.Model):
    _inherit='res.partner'
    _name = 'academia.persona'
    _description = 'Personas da academia'

    dni = fields.Char('Dni', required=True)
    curso_ids=fields.Many2many('academia.curso',string="Cursos",readonly=True)

    _sql_constraints = [('dni_uniq', 'UNIQUE (dni)', 'Non podes engadir outro rexistro co mesmo DNI')]
