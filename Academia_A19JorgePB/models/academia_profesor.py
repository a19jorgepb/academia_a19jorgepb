# -*- coding: utf-8 -*-
import logging

from odoo import models, fields, api


class AcademiaProfesor(models.Model):
    _inherit='academia.persona'
    _name = 'academia.profesor'
    _description = 'Estudiantes da acacemia'

    titulacion=fields.Text('Titulación')
