import logging

from odoo import models, fields, api


class AcademiaHorario(models.Model):
    _name = 'academia.horario'
    _description = 'Horarios da academia'
 

    #horario_id=fields.Text("Horario")
    
    dia=fields.Selection(
        [('Lunes','Lunes'),
        ('Martes','Martes'),
        ('Miercoles','Miercoles'),
        ('Jueves','Jueves'),
        ('Viernes','Viernes'),
        ('Sabado','Sabado'),
        ('Domingo','Domingo')],required=True
    )
    hora=fields.Selection(
        [('4','4'),
        ('5','6')],required=True
    )
    aula_id=fields.Many2one('academia.aula',ondelete='set null',string="Aula",index=True,required=True)

    curso =  fields.Many2many('academia.curso',domain=[('is_terminado','=',False)],readonly=True)





