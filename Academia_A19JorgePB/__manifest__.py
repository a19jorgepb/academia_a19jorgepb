# -*- coding: utf-8 -*-
{
    'name': "Academia_A19JorgePB",  # Module title
    'description': """Modulo xestión academia""",  # You can also rst format
    'author': "A19JorgePB",
    'website': "http://www.example.com",
    'category': 'Uncategorized',
    'version': '12.0.1',
    'depends': ['base'],
    'data': [
        'security/ir.model.access.csv',
        'views/academia_estudiante.xml',
        'views/academia_curso.xml',
        'views/academia_aula.xml',
        'views/academia_profesor.xml',
        'views/academia_horario.xml',
        
    ],
}
