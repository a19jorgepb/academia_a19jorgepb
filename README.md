## Descrición:

Este módulo serve para levar a xestión dunha academia. A cal quere ter unha información acerca dos cursos que se imparten, así como dos estudantes que realizan os cursos e dos profesores que imparten eses cursos. Nesta academia cada curso é impartido por un profesor e nunha determinada aula. Tamén leva o control dos horarios para saber que clase está ocupada e cal está libre.
Polo tanto, en principio o obxectivo deste módulo é mostrar o funcionamento dunha academia.



## Documentación:
   
Este módulo consta de 6 modelos: Persoa, Profesor, Estudante, Aula, Horario e Curso.
Persoa:  Este modelo herda do modelo res.partner, polo que os campos que vai a herdar dese modelo son: name, image, mobile, street e email. A maiores ten un campo DNI cunha Constraint SQL para que o campo DNI non se poda repetir e un campo relacional de tipo Many2many asociado ao modelo Curso para saber que cursos asociados ten.
- Profesor: Este modelo herda do modelo Persoa polo que vai ter todos os campos anteriores máis un campo titulación. Consta de dous tipos de vistas asociadas, unha de tipo árbol, onde se mostra un listados dos profesores  co seu DNI, nome e teléfono e outra vista de tipo formulario, onde aparecen os datos anteriores máis unha foto, a dirección, o email e unha lista de só lectura dos cursos que imparte cos campos definidos na vista de tipo árbol do modelo curso. A maiores tamén ten unha vista de tipo search para filtrar polo nome e a titulación.
- Estudante: Este modelo tamén herda do modelo Persoa e ten dúas vistas asociadas parecidas a dos profesores e a maiores ten unha vista de tipo search para filtrar os alumnos por nome e por idade. O campo idade e calculado a partir da fecha de nacemento.
- Aula: O modelo consta dos seguintes campos: nome da aula, descrición e  ubicación.
- Horario: Através deste modelo lévase a xestión dos horarios nas aulas. Ten tres campos: día, hora e un campo Many2One asociado ao modelo aula.
- Curso: Os campos deste modelo son nome do curso, descrición, data de inicio e de fin, estado(para saber si un curso está activo ou finalizado), profesor de tipo Many2one asociado ao modelo profesor e estudiantes de tipo Many2Many asociado ao modelo alumno. Ademais, ten un campo de tipo enteiro chamado aforo o cal indica o aforo máximo do curso, e un campo calculado para mostrar o aforo ocupado que ten o curso a partir dos estudantes que conforman ese curso. Logo, créase unha constrain de maneira que si varía o aforo ou os estudantes, comprobe que o aforo é positivo e o número de estudantes non sexa maior ao aforo. A parte de todo esto, tamén ten un campo Many2Many asociado ao modelo horario e aquí realízase unha constrain para que non se poda añadir horarios repetidos en canto ós cursos que están activos e tamén comproba que os profesores non poida estar dando clase o mesmo día e na mesma hora pero en diferentes aulas. Este modelo ten tres vistas asociadas, unha de tipo árbol, onde se mostra o nome do curso, a descrición, o profesor que imparte o curso, o aforo, e o aforo ocupado mediante un progressbar; outra de tipo formulario, onde aparece todo o anterior máis a data de inicio e fin e unha lista de estudantes; e outra vista de tipo gráfico, onde mostra os cursos e o aforo que ten.


## Funcionalidad:

Neste módulo pódese crear alumnos, profesores, aulas, cursos e horarios. Os horarios van ter o día, a hora e a aula. Cando se crea un curso, este vai empezar coa data actual de cando se cre. Neste curso vaise definir os horarios, o aforo e os alumnos que participen nel. Se os horarios ou horario definido son iguais aos horarios dalgún curso activo non se deixará engadir ese horario ou horarios e tampouco se deixará  que os profesores poidan estar dando clase o mesmo día e na mesma hora pero en diferentes aulas. En canto ós alumnos e profesores, ambos terán os seu propios campos e unha lista dos cursos ós cales pertencen.  


